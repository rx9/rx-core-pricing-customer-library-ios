# RXCorePricingCustomer

A description of this package.

## Installation

RXCorePricingCustomer requires Xcode 12 or higher to build.

### Swift Package Manager

1. To start adding the RXCorePricingCustomer library to your iOS project, open your project in Xcode and select **File > Add Packages...**.
1. Enter the RXCorePricingCustomer iOS GitHub repo URL (`https://gitlab.com/rx9/rx-core-pricing-customer-library-ios`) into the search bar.
1. You'll see the RXCorePricingCustomer repository rules for which version you want Swift Package Manager to install. Choose the Exact Version dependency rule, as it will use the latest compatible version of the dependency that can be detected from the `main` branch, then press **Add Package**.
  ![Add package dependency](readme-images/01-add-package.png)
1. Make sure the correct target is selected and press **Add Package**.
  ![Select target](readme-images/02-select-target.png)
1. In your app code, explicitly import the Framework
```swift
import RXCorePricingCustomer
```
